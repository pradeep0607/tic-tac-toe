import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-logic',
  templateUrl: './logic.component.html',
  styleUrls: ['./logic.component.css']
})
export class LogicComponent implements OnInit {
  userForm=FormGroup
  gametie:boolean=false;
  squares: string[]=[];
  xIsNext: boolean=false;
  winner: any='';
  userType :any=[
  'X','Y'
  ]
  
users =new FormGroup({
  usr: new FormControl('',Validators.required)
});
get u(){
return this.users.controls;

}
sub(){
  console.log(this.users.value)
}
changUser(e:any){
  console.log(e.target.value)
}

// end user
  constructor( ) {}

  ngOnInit() {
    this.newGame();
  }
  
  newGame() {
    this.squares = Array(9).fill(null);
    this.winner = null;
    this.xIsNext = true;
    this.gametie=false;
  }

  get player() {
    return this.xIsNext ? 'X' : 'O';
  }

  makeMove(idx: number) {
    if (!this.squares[idx]) {
      this.squares.splice(idx, 1, this.player);
      this.xIsNext = !this.xIsNext;
    }
  
    this.winner = this.calculateWinner();
  }


  calculateWinner() {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (
        this.squares[a] &&
        this.squares[a] === this.squares[b] &&
        this.squares[a] === this.squares[c]
        
      ) {
        // this.gametie=true;
        this.winner =this.squares[a];
        return this.winner;
      }
        // this.gameover =true;
        // this.winner = this.winner[a];
       
    }
  let occupy:number = 0;
  this.squares.forEach((e) => {
   occupy += (e !== null ? 1 : 0) 
  });

  
  if (occupy === 9 ) {
   this.gametie=true ;
   this.winner ='tie'
  //  alert('match tie ');

}
   
   }
 
}
